package com.teamtwo.common;

public class PrintResult {

	public void printSuccessMessage(String successCode) {
		
		String successMessage = "";
		switch(successCode) {
			case "insert" : successMessage = "등록되었습니다."; break;
			case "update": successMessage  = "수정되었습니다.."; break;
			case "delete" : successMessage  = "삭제되었습니다."; break;
		}
		
		System.out.println(successMessage);
	}

	public void printErrorMessage(String errorCode) {
		
		String errorMessage = "";
		switch(errorCode) {
			case "insert" : errorMessage = "등록에 실패하였습니다."; break;
			case "update" : errorMessage = "수정에 실패하였습니다."; break;
			case "delete" : errorMessage = "삭제에 실패하였습니다."; break;
			case "select" : errorMessage = "검색에 실패하였습니다."; break;
		}
		
		System.out.println(errorMessage);
	}

	
}
