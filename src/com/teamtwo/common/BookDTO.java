package com.teamtwo.common;

public class BookDTO {

	private int bookCode; // pk키, 정수/ 책 코드(넘버)시퀀스 사용
	private String bookName; // 책 이름
	private String author; // 저자명
	private int price; // 가격
	private int categoryCode;
	public BookDTO() {
	}
	public BookDTO(int bookCode, String bookName, String author, int price, int categoryCode) {
		super();
		this.bookCode = bookCode;
		this.bookName = bookName;
		this.author = author;
		this.price = price;
		this.categoryCode = categoryCode;
	}
	public int getBookCode() {
		return bookCode;
	}
	public void setBookCode(int bookCode) {
		this.bookCode = bookCode;
	}
	public String getBookName() {
		return bookName;
	}
	public void setBookName(String bookName) {
		this.bookName = bookName;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getCategoryCode() {
		return categoryCode;
	}
	public void setCategoryCode(int categoryCode) {
		this.categoryCode = categoryCode;
	}
	@Override
	public String toString() {
		return "BookDTO [bookCode=" + bookCode + ", bookName=" + bookName + ", author=" + author + ", price=" + price
				+ ", categoryCode=" + categoryCode + "]";
	}
	
	
}
