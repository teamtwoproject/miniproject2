package com.teamtwo.common;

import java.util.List;

public class CategoryBookDTO {

	private int categoryCode; 
	private String categoryName; 
	private int refCategoryCode;
	List<BookDTO> bookList;
	
	public CategoryBookDTO() {
		super();
	}

	public CategoryBookDTO(int categoryCode, String categoryName, int refCategoryCode, List<BookDTO> bookList) {
		super();
		this.categoryCode = categoryCode;
		this.categoryName = categoryName;
		this.refCategoryCode = refCategoryCode;
		this.bookList = bookList;
	}

	public int getCategoryCode() {
		return categoryCode;
	}

	public void setCategoryCode(int categoryCode) {
		this.categoryCode = categoryCode;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public int getRefCategoryCode() {
		return refCategoryCode;
	}

	public void setRefCategoryCode(int refCategoryCode) {
		this.refCategoryCode = refCategoryCode;
	}

	public List<BookDTO> getBookList() {
		return bookList;
	}

	public void setBookList(List<BookDTO> bookList) {
		this.bookList = bookList;
	}

	@Override
	public String toString() {
		return "CategoryBookDTO [categoryCode=" + categoryCode + ", categoryName=" + categoryName + ", refCategoryCode="
				+ refCategoryCode + ", bookList=" + bookList + "]";
	}
	
	
}
