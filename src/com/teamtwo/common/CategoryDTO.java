package com.teamtwo.common;

public class CategoryDTO {

	private int categoryCode; // 카테고리코드
	private String categoryName; // 카테고리명
	private int refCategoryCode; // 참조할 카테고리 코드 
							// ex) 대분류 안에 소분류들 있으니까.
	public CategoryDTO() {
	}
	
	public CategoryDTO(int categoryCode, String categoryName, int refCategoryCode) {
		super();
		this.categoryCode = categoryCode;
		this.categoryName = categoryName;
		this.refCategoryCode = refCategoryCode;
	}

	public int getCategoryCode() {
		return categoryCode;
	}

	public void setCategoryCode(int categoryCode) {
		this.categoryCode = categoryCode;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public int getRefCategoryCode() {
		return refCategoryCode;
	}

	public void setRefCategoryCode(int refCategoryCode) {
		this.refCategoryCode = refCategoryCode;
	}

	@Override
	public String toString() {
		return "CategoryDTO [categoryCode=" + categoryCode + ", categoryName=" + categoryName + ", refCategoryCode="
				+ refCategoryCode + "]";
	}
}
