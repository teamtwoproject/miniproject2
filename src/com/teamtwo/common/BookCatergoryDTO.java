package com.teamtwo.common;

public class BookCatergoryDTO {
	private int bookCode;
	private String bookName;
	private String author;
	private int price;
	private CategoryDTO category;
	
	
	
	public BookCatergoryDTO() {
		super();
	}


	public BookCatergoryDTO(int bookCode, String bookName, String author, int price, CategoryDTO category) {
		super();
		this.bookCode = bookCode;
		this.bookName = bookName;
		this.author = author;
		this.price = price;
		this.category = category;
	}


	public int getBookCode() {
		return bookCode;
	}


	public void setBookCode(int bookCode) {
		this.bookCode = bookCode;
	}


	public String getBookName() {
		return bookName;
	}


	public void setBookName(String bookName) {
		this.bookName = bookName;
	}


	public String getAuthor() {
		return author;
	}


	public void setAuthor(String author) {
		this.author = author;
	}


	public int getPrice() {
		return price;
	}


	public void setPrice(int price) {
		this.price = price;
	}


	public CategoryDTO getCategory() {
		return category;
	}


	public void setCategory(CategoryDTO category) {
		this.category = category;
	}

	@Override
	public String toString() {
		return "BookCatrgoryDTO [bookCode=" + bookCode + ", bookName=" + bookName + ", author=" + author + ", price="
				+ price + ", category=" + category + "]";
	}
	
	
}
