package com.teamtwo.library;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import com.teamtwo.common.BookDTO;
import com.teamtwo.common.SearchCriteria;

/* 400 자연과학 전용 뷰 페이지입니다. */
public class NatureScienceView {

	Scanner sc = new Scanner(System.in);

	public void NatureScienceMainView() {
		
		NatureScienceView natureScience = new NatureScienceView();
		LibraryService libraryService = new LibraryService();
		Application app = new Application();

		
		do {
			System.out.println("");
			System.out.println("===== 400 자연과학 작업 메뉴입니다. =====");
			System.out.println("1. 자연과학 책 조회하기");
			System.out.println("2. 자연과학 책 정보 추가하기");
			System.out.println("3. 자연과학 책 정보 수정하기");
			System.out.println("4. 자연과학 책 정보 삭제하기");
			System.out.println("5. 메인 메뉴로 돌아가기");
			System.out.print("원하시는 메뉴를 선택하세요 : ");
			
			int no = sc.nextInt();
			System.out.println("");
			
			switch(no) {
			case 1: natureScience.SelectBook(); break;
			case 2: libraryService.RegistBook(natureScience.inputBook()); break;
			case 3: libraryService.ModifyBook(natureScience.inputModifyBook()); break;
			case 4: libraryService.DeleteBook1(natureScience.inputBookCode()); break;
			case 5: app.main(null);
			default : System.out.print("올바른 메뉴 번호를 입력하세요 (1~5) : "); break;
			}
			
		} while(true);
		
	}
	


	private void SelectBook() {
		
		NatureScienceView natureScience = new NatureScienceView();
		LibraryService libraryService = new LibraryService();

		do {  
			System.out.println("");
			System.out.println("===== 400 자연과학 책 '조회' 메뉴입니다. =====");
			System.out.println("1. 모든 자연과학 책 조회하기");
			System.out.println("2. 책 이름, 카테고리, 저자로 검색하기");
			System.out.println("3. 가격대별 책 조회하기");
			System.out.println("4. 오늘의 추천도서 (랜덤으로 3권 조회)");
			System.out.println("5. 상위 메뉴로 돌아가기");
			System.out.print("원하시는 메뉴를 선택하세요 : ");
			
			int no = sc.nextInt();
			System.out.println("");
			
			switch(no) {
			case 1: libraryService.selectAllNatureScienceBook(); break;
			case 2: libraryService.selectNatureScienceBookByColumn(natureScience.inputSearchCriteriaMap()); break;
			case 3: libraryService.selectNatureScienceBookByPrice(natureScience.inputPrice()); break;
			case 4: libraryService.selectNatureScienceBookByRandom(); break;
			case 5: return;
			default : System.out.print("올바른 메뉴 번호를 입력하세요 (1~5) : "); break;
			}
			
		} while(true);
		
	}

	

	private static Map<String, Object> inputSearchCriteriaMap() {
		
		LibraryService libraryService = new LibraryService();
		Scanner sc = new Scanner(System.in);

		System.out.print("검색 기준을 입력해주세요(bookname 또는 category 또는 author) : ");
		String condition = sc.nextLine();
		
		Map<String, Object> criteria = new HashMap<>();
		if("bookname".equals(condition)) {
			
			System.out.print("검색할 책 이름을 입력하세요 : ");
			String nameValue = sc.nextLine();
			criteria.put("nameValue", nameValue);

		} else if("category".equals(condition)) {
			
			libraryService.selectAllNatureScienceCategory();
			System.out.print("위의 카테고리 목록을 참고하여, 검색할 카테고리 코드를 입력하세요 : ");
			int categoryValue = sc.nextInt();			
			criteria.put("categoryValue", categoryValue);
			
		} else if("author".equals(condition)) {
			
			System.out.print("검색할 저자 이름을 입력하세요 : ");
			String authorValue = sc.nextLine();
			criteria.put("authorValue", authorValue);
			
		}
		
		return criteria;
		
	}

	
	
	private static int inputPrice() {
		
		Scanner sc = new Scanner(System.in);
		System.out.print("원하시는 가격대를 입력해주세요 (ex> 2만원대 = 2) : ");
		
		int price = sc.nextInt();
		
		return price;
	}
	
	
	
	private static BookDTO inputBook() {
		
		Scanner sc = new Scanner(System.in);
		NatureScienceView natureScience = new NatureScienceView();
		LibraryService libraryService = new LibraryService();


		String bookName;
		String author;
		int price;
		int categoryCode;
		

		System.out.println("");
		System.out.println("자연과학 책 추가 메뉴를 선택하셨습니다.");
		System.out.println("");
		System.out.println("상위 메뉴로 되돌아가시려면 숫자 '5'를 입력하시고,");
		System.out.print("책 정보를 추가하시려면 책 이름을 입력하세요 : ");
		bookName = sc.nextLine();
		
		if(natureScience.isInt(bookName)) {
			natureScience.NatureScienceMainView();
			return null;
		} else {
		System.out.println("");
		System.out.print("책 저자(출판사)를 입력하세요 (한글 6자까지) : ");
		author = sc.nextLine();
		System.out.println("");
		System.out.print("책 가격을 입력하세요 : ");
		price = sc.nextInt();
		System.out.println("");
		System.out.println("< 카테고리 코드 >");
		System.out.println("수학 : 410");
		System.out.println("물리학 : 420");
		System.out.println("화학 : 430");
		System.out.println("천문학 : 440");
		System.out.println("지학 : 450");
		System.out.println("광물학 : 460");
		System.out.println("생명과학 : 470");
		System.out.println("식물학 : 480");
		System.out.println("동물학 : 490");
		System.out.println("");
		System.out.print("카테고리 코드를 입력하세요 : ");		
		categoryCode = sc.nextInt();
		
		BookDTO book = new BookDTO();
		book.setBookName(bookName);
		book.setAuthor(author);
		book.setPrice(price);
		book.setCategoryCode(categoryCode);
		
		return book;
		}
	}
			
	
			
	private boolean isInt(String bookName) {
		try{
		int x = Integer.parseInt(bookName);
		return true;
		} catch (NumberFormatException e) {
		return false;
		}		
	}
	
	
	private static Map<String, String> inputModifyBook() {
		
		Scanner sc = new Scanner(System.in);
		NatureScienceView natureScience = new NatureScienceView();
		LibraryService libraryService = new LibraryService();

		System.out.println("");
		System.out.println("자연과학 책 수정 메뉴를 선택하셨습니다.");
		System.out.println("");
		libraryService.selectAllNatureScienceBook();
		System.out.println("상위 메뉴로 돌아가시려면 정수가 아닌 값을 입력하시고,");
		System.out.print("책 정보를 수정하시려면, 수정할 책의 코드(정수)를 입력하세요 : ");		
		String bookCode = sc.nextLine();
		
		if (Character.isDigit(bookCode.charAt(0))) {
		System.out.print("수정할 책의 이름을 입력하세요 : ");
		String bookName = sc.nextLine();
		System.out.print("수정할 책의 저자를 입력하세요 : ");
		String author = sc.nextLine();
		System.out.print("수정할 책의 가격을 입력하세요 : ");
		String price = sc.nextLine();
		System.out.print("수정할 책의 카테고리 코드를 입력하세요 : ");
		String categoryCode = sc.nextLine();
				
		Map<String, String> parameter = new HashMap<>();
		parameter.put("bookCode", bookCode);
		parameter.put("bookName", bookName);
		parameter.put("author", author);
		parameter.put("price", price);
		parameter.put("categoryCode", categoryCode);
				
		return parameter;
		} else {
			natureScience.NatureScienceMainView();
		}		
		return null;
	}



	private static Map<String, String> inputBookCode() {
		
		Scanner sc = new Scanner(System.in);
		LibraryService libraryService = new LibraryService();

		
		libraryService.selectAllNatureScienceBook();
		System.out.print("위 리스트를 참고하여, 삭제할 책 코드를 입력하세요 : ");
		String bookCode = sc.nextLine();
		
		Map<String, String> parameter = new HashMap<>();
		parameter.put("bookCode", bookCode);
		
		return parameter;

	}

}
