package com.teamtwo.library;

import static com.teamtwo.common.Template.getSqlSession;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.ibatis.session.SqlSession;

import com.teamtwo.common.BookCatergoryDTO;
import com.teamtwo.common.BookDTO;
import com.teamtwo.common.CategoryBookDTO;
import com.teamtwo.common.CategoryDTO;
import com.teamtwo.common.SearchCriteria;

public class LibraryService {

	private libraryMapper mapper;
	private libraryMapper libraryMapper;

    private final PrintResult printResult = new PrintResult();
// 상목꺼 시작
    
	public void selectAllCategory() {   // 모든 카테고리 출력을 위한 메소드
		
		SqlSession sqlSession = getSqlSession();
		mapper = sqlSession.getMapper(libraryMapper.class);
		
		List<CategoryDTO> categoryList = mapper.selectAllCategory();

		for(CategoryDTO categoryDTO : categoryList) {
			System.out.println(categoryDTO);
			
		}
			sqlSession.close();
	}
	
	public void selectAllBook(Map<String, Object> criteria) {
		
		SqlSession sqlSession = getSqlSession();
		mapper = sqlSession.getMapper(libraryMapper.class);
		
		List<BookDTO> bookList = mapper.selectAllBook(criteria);
		
		for(BookDTO bookDTO : bookList) {
			System.out.println(bookDTO);
		}
		sqlSession.close(); 
	}

	public void insertCategory(CategoryDTO category) {
		
		SqlSession sqlSession = getSqlSession();
		mapper = sqlSession.getMapper(libraryMapper.class);
		
		int result = mapper.insertNewCategory(category);
		

		if(result > 0) {
			
			System.out.println("신규 카테고리 등록에 성공하셨습니다.");
			sqlSession.commit();
		}else {
			
			System.out.println("신규 카테고리 등록에 실패하셨습니다.");
			sqlSession.rollback();
		}

		sqlSession.close();
		
	}

	public void insertBook(BookDTO book) {
		
		SqlSession sqlSession = getSqlSession();
		mapper = sqlSession.getMapper(libraryMapper.class);
		
		int result = mapper.insertNewBook(book);
		
		if(result > 0) {
			
			System.out.println("신규 도서 등록에 성공하셨습니다.");
			sqlSession.commit();
		}else {
			
			System.out.println("신규 도서 등록에 실패하셨습니다.");
			sqlSession.rollback();
		}

		sqlSession.close();
		
	}


	public void modifyCategory(Map<String, Object> criteria) {
//		상목
		SqlSession sqlSession = getSqlSession();
		mapper = sqlSession.getMapper(libraryMapper.class);
		
		int result = mapper.modifyCategory(criteria);
		
		if(result > 0) {
			System.out.println("변경에 성공하셨습니다 !");
			sqlSession.commit();
		} else {
			System.out.println("변경에 실패하셨습니다 ㅠ");
			sqlSession.rollback();
		}
		sqlSession.close();
	}


	public void modifyBook(Map<String, Object> criteria) {
//		상목
		SqlSession sqlSession = getSqlSession();
		mapper = sqlSession.getMapper(libraryMapper.class);
		
		int result = mapper.modifyBook(criteria);
		
		if(result > 0) {
			System.out.println("카테고리 변경에 성공하셨습니다 !");
			sqlSession.commit();
		} else {
			System.out.println("카테고리 변경에 실패하셨습니다 ㅠ");
			sqlSession.rollback();
		}
		sqlSession.close();
	}

	public void deleteBook(Map<String, Object> bookCode) {
//		상목
		SqlSession sqlSession = getSqlSession();
		mapper = sqlSession.getMapper(libraryMapper.class);
		
		int result = mapper.deleteBook(bookCode);
		
		if(result > 0) {
			System.out.println("도서 삭제에 성공하셨습니다 !");
			sqlSession.commit();
		} else {
			System.out.println("도서 삭제에 실패하셨습니다 ㅠ");
			sqlSession.rollback();
		}
		sqlSession.close();
	}

// 상목거 끝
	public void searchLiteratureBook(SearchCriteria searchCriteria) {
		
		SqlSession sqlSession = getSqlSession();
		mapper = sqlSession.getMapper(libraryMapper.class);
		
		List<BookDTO> bookList = mapper.searchMenu(searchCriteria);
		
		if(bookList != null && bookList.size() > 0) {
			
			for(BookDTO menu : bookList) {
				System.out.println(menu);
			}
		} else {
			
			System.out.println("검색 결과가 존재하지 않습니다.");
		}
		
		sqlSession.close();
	}


	public void registBook(Map<String, Object> criteria) {
				
			SqlSession sqlSession = getSqlSession();
			
			mapper = sqlSession.getMapper(libraryMapper.class);
			
			int result = mapper.registBook(criteria);
			
			if(result > 0) {
				
				System.out.println("등록에 성공하였습니다.");
				sqlSession.commit();
			} else {
				
				System.out.println("등록에 실패하였습니다.");
				sqlSession.rollback();
			}
			
			sqlSession.close();
			
			
	}

	public void modifyBook1(Map<String, Object> criteria) {
		
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(libraryMapper.class);
		
		int result = mapper.modifyBook1(criteria);
		
		if(result > 0) {
			
			System.out.println("수정에 성공하였습니다.");
			sqlSession.commit();
		} else {
			
			System.out.println("수정에 실패하였습니다.");
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
	}


	public void deleteBook1(Map<String, Object> criteria) {
		
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(libraryMapper.class);
		
		int result = mapper.deleteBook1(criteria);
		
		if(result > 0) {
			
			System.out.println("삭제에 성공하였습니다.");
			sqlSession.commit();
		} else {
			
			System.out.println("삭제에 실패하였습니다.");
			sqlSession.rollback();
		}
		
		sqlSession.close();
	}
		
	public void AddCategory(Map<String, String> inputCategory) {

		SqlSession sqlSession = getSqlSession();
		mapper = sqlSession.getMapper(libraryMapper.class);
		
		int result = mapper.AddCategory(inputCategory);
		
		if(result > 0) {
			
			printResult.printSuccessMessage("insert");
			sqlSession.commit();
		} else {
			
			printResult.printErrorMessage("insert");
			sqlSession.rollback();
		}
		sqlSession.close();
	}

	public void AddBook(BookDTO bookInfo) {
		
		SqlSession sqlSession = getSqlSession();
		mapper = sqlSession.getMapper(libraryMapper.class);
		
		int result = mapper.AddBook(bookInfo);
		
		if(result > 0) {
			
			printResult.printSuccessMessage("insert");
			sqlSession.commit();
		} else {
			
			printResult.printErrorMessage("insert");
			sqlSession.rollback();
		}
		sqlSession.close();
	}

	public void AddcategoryBook(BookCatergoryDTO inputBoth) {
		
		SqlSession sqlSession = getSqlSession();
		mapper = sqlSession.getMapper(libraryMapper.class);
		
		Map<String, String> cateName = new HashMap<String, String>();
		
		String categoryName = inputBoth.getCategory().getCategoryName();
		
		cateName.put("categoryName", categoryName);
		
		int result1 = mapper.AddCategory(cateName);
		int result2 = mapper.AddcategoryBook(inputBoth);
		
		if(result1 > 0 && result2 > 0) {
			
			printResult.printSuccessMessage("insert");
			sqlSession.commit();
		} else {
			
			printResult.printErrorMessage("insert");
			sqlSession.rollback();
		}
		sqlSession.close();
	}

	public void selectCategory() {
		
		SqlSession sqlSession = getSqlSession();
		mapper = sqlSession.getMapper(libraryMapper.class);
		
		
		List<CategoryDTO> categoryList = mapper.selectCategory();
		
				
		
		if(categoryList.size() > 0 && categoryList != null) {
			
			for(CategoryDTO category : categoryList) {
				
				System.out.println(category);
			}
				
		} else {
			
			printResult.printErrorMessage("select");
		}
		sqlSession.close();
	}

	public List<CategoryDTO> getSubCategory(char selectCategory) {
//		AddBookInCategory와 이어진 서브메소드
		SqlSession sqlSession = getSqlSession();
		mapper = sqlSession.getMapper(libraryMapper.class);
		
		
		List<CategoryDTO> categoryList = mapper.getSubCategory(selectCategory);
		
		
		if(categoryList.size() > 0 && categoryList != null) {
			
			
			for(CategoryDTO category : categoryList) {
				
				System.out.println(category);
			}
				
		} else {
			
			printResult.printErrorMessage("select");
		}
		sqlSession.close();
		return categoryList;
	}

	public void selectCategoryBook(int categoryCode) {
		/* 실행계획
		 * categoryCode를 받아서 세부 카테고리의 도서들을 불러온뒤 (where = CATEGORY_CODE = #{ categoryCode}
		 * List<Category> 로 받아, random메소드를 통해 index를 무작위로 추출해서 5개 보여주고,
		 * 1 : n 조인 collection을 이용해 출력.
		 * */
		SqlSession sqlSession = getSqlSession();
		mapper = sqlSession.getMapper(libraryMapper.class);
		
		Map<String, Integer> categoryCodeMap = new HashMap<String, Integer>();
		categoryCodeMap.put("categoryCode", categoryCode);
		
		List<CategoryBookDTO> categoryList = mapper.selectCategoryBook(categoryCode);
		
		if(categoryList.size() > 0 && categoryList != null) {
			
			for(CategoryBookDTO category : categoryList) {
				
				System.out.println(category.getBookList());
			}
				
		} else {
			
			printResult.printErrorMessage("select");
		}
		sqlSession.close();
	}

	public void recommandFive(int categoryCode) {
		
		SqlSession sqlSession = getSqlSession();
		mapper = sqlSession.getMapper(libraryMapper.class);
		
		Map<String, Object> categoryCodeMap = new HashMap<>();
		
		Set<Integer> ranSet = new HashSet<>();
		for(int i = 0; ranSet.size() < 5; i++) {
			
			int ranNum = (int)(Math.random()*117 + 1);
			ranSet.add(ranNum);
		}
		
		List<Integer> ranList = new ArrayList<>(ranSet);
		categoryCodeMap.put("ranList", ranList);
		categoryCodeMap.put("categoryCode", categoryCode);
		List<BookCatergoryDTO> categoryList = mapper.recommandFive(categoryCodeMap);
		
		if(categoryList.size() > 0 && categoryList != null) {
			
			for(BookCatergoryDTO BookRan : categoryList) {
				
				System.out.println(BookRan);
			}
				
		} else {
			
			printResult.printErrorMessage("select");
		}
		sqlSession.close();
	}

	
	
	/* 400 자연과학 메소드 시작 부분입니다. */
	
	public void selectAllNatureScienceBook() {
		
		SqlSession sqlSession = getSqlSession();
		
		libraryMapper = sqlSession.getMapper(libraryMapper.class);
		List<BookDTO> bookList = libraryMapper.selectAllNatureScienceBook();
		
		sqlSession.close();
		
		
		if(bookList != null) {
			
			for(BookDTO book : bookList) {
				System.out.println(book);
			}
		} else {
			
			System.out.println("자연과학 모든 책 조회에 실패하였습니다...");
		}
	}


	
	public void selectAllNatureScienceCategory() {
		
		SqlSession sqlSession = getSqlSession();
		
		libraryMapper = sqlSession.getMapper(libraryMapper.class);
		List<CategoryDTO> categoryList = libraryMapper.selectAllNatureScienceCategory();
		
		sqlSession.close();
		
		
		if(categoryList != null) {
			
			for(CategoryDTO category : categoryList) {
				System.out.println(category);
			}
		} else {
			
			System.out.println("자연과학 모든 카테고리 조회에 실패하였습니다...");
		}
	}
	

	
	public void selectNatureScienceBookByColumn(Map<String, Object> criteria) {
		
		SqlSession sqlSession = getSqlSession();
		libraryMapper = sqlSession.getMapper(libraryMapper.class);
		
		List<BookDTO> bookList = libraryMapper.selectNatureScienceBookByColumn(criteria);
		
		if(bookList != null && bookList.size() > 0) {
			
			for(BookDTO book : bookList) {
				System.out.println(book);
			}
			System.out.println(criteria+"의 조건으로 검색을 성공하였습니다!");
		} else {
			
			System.out.println("조건으로 책 검색이 실패하였습니다...");
		}
		sqlSession.close();
	}	

	
	
	public void selectNatureScienceBookByPrice(int inputPrice) {
		
		SqlSession sqlSession = getSqlSession();
		libraryMapper = sqlSession.getMapper(libraryMapper.class);

		Map<String, Integer> price = new HashMap<>();
		price.put("price", inputPrice);
		
		List<BookDTO> bookList = libraryMapper.selectNatureScienceBookByPrice(price);
		
		if(bookList != null && bookList.size() > 0) {
			
			for(BookDTO book : bookList) {
				System.out.println(book);
			}
			System.out.println(inputPrice+"만원대 자연과학 서적 검색을 성공하였습니다!");

		} else {
			
			System.out.println("원하시는 가격대의 책이 없습니다...");
		}
		sqlSession.close(); 
	}

	
	
	public void selectNatureScienceBookByRandom() {
		
		SqlSession sqlSession = getSqlSession();
		libraryMapper = sqlSession.getMapper(libraryMapper.class);

		List<BookDTO> bookList = libraryMapper.selectNatureScienceBookByRandom();
		
		if(bookList != null && bookList.size() > 0) {
			
			for(BookDTO book : bookList) {
				System.out.println(book);
			}
			System.out.println("오늘의 추천도서 3권 추출 성공! 읽어보세요~");

		} else {
			
			System.out.println("오늘의 추천도서 추출에 실패하였습니다...");
		}
		sqlSession.close(); 
		
		
	}

	
	
	public void RegistBook(BookDTO book) {
		
		System.out.println("");

		SqlSession sqlSession = getSqlSession();
		BookDTO bookDTO = new BookDTO(); 
		
		libraryMapper = sqlSession.getMapper(libraryMapper.class);
		int result = libraryMapper.insertBook(book);
		
		if(result > 0) {
			
			List<String> currBook = new ArrayList<String>();
			currBook.add(book.getBookName());
			currBook.add(book.getAuthor());
			currBook.add(String.valueOf(book.getPrice()));
			currBook.add(String.valueOf(book.getCategoryCode()));
			
			for(String book1 : currBook) {
				System.out.print(book1+", ");
			}
			System.out.println("");
			System.out.println("위의 신규 자연과학 도서 추가를 완료하였습니다.");
			sqlSession.commit();
			
		} else {
			
			System.out.println("자연과학 도서 추가를 실패하였습니다...");
			sqlSession.rollback();
		}
		sqlSession.close();
	}



	public void ModifyBook(Map<String, String> inputModifyBook) {
//		보성님
		
		int bookCode = Integer.parseInt(inputModifyBook.get("bookCode"));
		String bookName = inputModifyBook.get("bookName");
		String author = inputModifyBook.get("author");
		int price = Integer.parseInt(inputModifyBook.get("price"));
		int categoryCode = Integer.parseInt(inputModifyBook.get("categoryCode"));
		
		BookDTO book = new BookDTO();
		book.setBookCode(bookCode);
		book.setBookName(bookName);
		book.setAuthor(author);
		book.setPrice(price);
		book.setCategoryCode(categoryCode);
		
		SqlSession sqlSession = getSqlSession();
		
		libraryMapper = sqlSession.getMapper(libraryMapper.class);
		int result = libraryMapper.modifyNatureScienceBook(book);
		
		if(result > 0) {
			
			List<String> currBook = new ArrayList<String>();
			currBook.add(book.getBookName());
			currBook.add(book.getAuthor());
			currBook.add(String.valueOf(book.getPrice()));
			currBook.add(String.valueOf(book.getCategoryCode()));
			
			for(String book1 : currBook) {
				System.out.print(book1+", ");
			}
			System.out.println("위의 자연과학 책 정보 수정을 성공하였습니다.");
			sqlSession.commit();
			
		} else {
			
			System.out.println("자연과학 책 정보 수정을 실패하였습니다...");
			sqlSession.rollback();
		}
		sqlSession.close();
	}

	
	
	public void DeleteBook1(Map<String, String> inputBookCode) {
		
		int bookCode = Integer.parseInt(inputBookCode.get("bookCode"));		
		
		SqlSession sqlSession = getSqlSession();

		libraryMapper = sqlSession.getMapper(libraryMapper.class);
		int result = libraryMapper.deleteNatureScienceBook(bookCode);
		
		if(result>0)	{
			System.out.println("");
			System.out.println("책 코드 : "+bookCode+" 에 대한 정보가 삭제되었습니다.");
			System.out.println("");
			sqlSession.commit();
			
		} else {
			System.out.println("해당 책 정보 삭제에 실패하였습니다...");
			sqlSession.rollback();
		}
		sqlSession.close();		
	}

	/* 400 자연과학 메소드 끝 부분입니다. */

}
