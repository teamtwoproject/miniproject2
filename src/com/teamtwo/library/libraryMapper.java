package com.teamtwo.library;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import com.teamtwo.common.BookDTO;
import com.teamtwo.common.SearchCriteria;
import com.teamtwo.common.BookCatergoryDTO;
import com.teamtwo.common.CategoryBookDTO;
import com.teamtwo.common.CategoryDTO;

public interface libraryMapper {
	


	int AddCategory(Map<String, String> inputCategory);

	int AddBook(BookDTO bookInfo);

	int AddcategoryBook(BookCatergoryDTO inputBoth);

	List<CategoryDTO> selectCategory();

	List<CategoryDTO> getSubCategory(char selectCategory);

	ArrayList<CategoryBookDTO> selectCategoryBook(int categoryCode);

	List<BookCatergoryDTO> recommandFive(Map<String, Object> categoryCodeMap);


	List<BookDTO> searchMenu(SearchCriteria searchCriteria);

	int registBook(Map<String, Object> criteria);
	
	int modifyBook1(Map<String, Object> criteria);

	int deleteBook1(Map<String, Object> criteria);
	
	
	/* 400 자연과학 부분입니다 */
	
	List<BookDTO> selectAllNatureScienceBook();

	List<CategoryDTO> selectAllNatureScienceCategory();

	int insertBook(BookDTO book);

	List<BookDTO> selectLastRegisteredNatureScienceBook();

	int modifyNatureScienceBook(BookDTO book);

	int deleteNatureScienceBook(int bookCode);

	List<BookDTO> selectNatureScienceBookByColumn(Map<String, Object> criteria);

	List<BookDTO> selectNatureScienceBookByPrice(Map<String, Integer> price);

	List<BookDTO> selectNatureScienceBookByRandom();

	/* 400 자연과학 부분입니다 */
	
	/* 900 역사 부분 시작 */
	List<CategoryDTO> selectAllCategory();

	List<BookDTO> selectAllBook(Map<String, Object> criteria);

	int insertNewCategory(CategoryDTO category);

	int insertNewBook(BookDTO book);

	int modifyCategory(Map<String, Object> criteria);

	int modifyBook(Map<String, Object> criteria);

	int deleteBook(Map<String, Object> bookCode);

	
	
	
	
}