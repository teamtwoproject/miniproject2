package com.teamtwo.library;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.teamtwo.common.BookCatergoryDTO;
import com.teamtwo.common.BookDTO;
import com.teamtwo.common.CategoryDTO;
import com.teamtwo.common.SearchCriteria;

public class Application {

	static LibraryService libraryService = new LibraryService();
	static NatureScienceView natureScienceView = new NatureScienceView();

	static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		
		do {  
			System.out.println("========== 도서 관리 프로그램 ==========");
			System.out.println("1. 400 자연과학 by보성");
			System.out.println("2. 500 기술과학 by도원 (미구현)");
			System.out.println("3. 600 예술 by효진 (미구현)");
			System.out.println("4. 700 언어 by성일");
			System.out.println("5. 800 문학 by현구");
			System.out.println("6. 900 역사 by상목"); /* 59줄 부터 257까지 */
			System.out.println("9. 종료하기");
			System.out.println("");
			System.out.print("원하시는 분류를 선택하세요 : ");
			
			int no = sc.nextInt();
			sc.nextLine();
			System.out.println("");
			
			switch(no) {
			case 1: natureScienceView.NatureScienceMainView(); break;
			case 2: /* 도원님 개인작업 */; break;
			case 3: /* 효진님 개인작업 */; break;
			case 4: LanguageSection(); break;
			case 5: literatureMenu(); break;
			case 6: historyMenu(); break;
			case 9:  System.out.println("프로그램을 종료합니다."); break;
			default : System.out.println("올바른 값을 입력하세요."); break;
			}
			
		} while(true);
		
	}
/* 상목 메소드 시작   */
	public static void historyMenu() {
		
		Scanner sc = new Scanner(System.in);
		LibraryService libraryService = new LibraryService();
		do {
			
			System.out.println("========= 900 역사 ==========");
			System.out.println("1. 카테고리 및 도서 목록 보기");
			System.out.println("2. 카테고리 및 도서 추가");
			System.out.println("3. 카테고리 및 도서 변경");
			System.out.println("4. 도서 삭제");
			System.out.println("9. 대분류 메뉴로 이동하기");
			System.out.print("메뉴를 선택해주세요 :");
			int no = sc.nextInt();
			sc.nextLine();
			
			switch(no) {
			
			case 1: selectSubMenu(); break; // 모든 카테고리 출력
			case 2: insertSubMenu(); break; // 카테고리 및 도서 추가
			case 3: modifySubMenu(); break; // 변경을 위한 서브메뉴
			case 4: deleteSubMenu(); break; // 삭제를 위한 서브메뉴
			default: System.out.println("잘못 입력하셨습니다. 다시 시도해주세요 !");
					
			} if(no == 9) {
				break;
			}
			
		}while(true);
	}

	private static void selectSubMenu() {
		
		Scanner sc = new Scanner(System.in);
		LibraryService libraryService = new LibraryService();
		
		do {
			System.out.println("===== 카테고리나 도서의 추가를 위한 메뉴입니다. =====");
			System.out.println("1. 전체 카테고리 조회하기");
			System.out.println("2. 카테고리별 도서 조회하기");
			System.out.println("9. 이전 메뉴로");
			System.out.print("메뉴를 선택해주세요 : ");
			
			int no = sc.nextInt();
		
			switch(no) {
			case 1: libraryService.selectAllCategory();; break;
			case 2: libraryService.selectAllBook(inputSelectCode()); break;
			}
			if(no == 9) {
				return;
			}
		}while(true);
	}
	
	private static Map<String, Object> inputSelectCode() {
		
		Scanner sc = new Scanner(System.in);
		System.out.print("도서 목록을 볼 카테고리를 코드를 입력해주세요.(카테고리별 도서목록) : ");
		int categoryCode = sc.nextInt();
		
		Map<String, Object> criteria = new HashMap<>();
		
		criteria.put("categoryCode", categoryCode);
		return criteria;
		
	}

	private static void insertSubMenu() {
		
		Scanner sc = new Scanner(System.in);
		LibraryService libraryService = new LibraryService();
		
		do {
			System.out.println("===== 카테고리나 도서의 추가를 위한 메뉴입니다. =====");
			System.out.println("1. 카테고리 추가하기");
			System.out.println("2. 도서 추가하기");
			System.out.println("9. 이전 메뉴로");
			System.out.print("메뉴를 선택해주세요 : ");
			
			int no = sc.nextInt();
		
			switch(no) {
			case 1: libraryService.insertCategory(inputNewCategory()); break;
			case 2: libraryService.insertBook(inputNewBook()); break;
			}
			if(no == 9) {
				return;
			}
		}while(true);
	}
	
	

	private static CategoryDTO inputNewCategory() {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("등록하실 카테고리 이름을 입력해주세요 : ");
		String name = sc.nextLine();
		System.out.println("참조될 카테고리 번호를 입력해주세요 : ");
		int no = sc.nextInt();
		CategoryDTO category = new CategoryDTO();
		category.setCategoryName(name);
		category.setRefCategoryCode(no);
		
		return category;
	}

	private static BookDTO inputNewBook() {
		
		Scanner sc = new Scanner(System.in);
		System.out.print("등록할 책의 코드번호를 입력하세요 : ");
		int code = sc.nextInt();
		sc.nextLine();
		System.out.print("등록할 책 이름을 입력하세요 : ");
		String name = sc.nextLine();
		System.out.print("등록할 책의 저자를 입력하세요 : ");
		String author = sc.nextLine();
		System.out.print("등록할 메뉴의 가격을 입력하세요 : ");
		int price = sc.nextInt();
		System.out.print("등록할 카테고리 코드를 입력하세요 : ");
		int categoryCode = sc.nextInt();
		sc.nextLine();
		
		BookDTO book = new BookDTO();
		book.setBookCode(code);
		book.setBookName(name);
		book.setAuthor(author);
		book.setPrice(price);
		book.setCategoryCode(categoryCode);
		
		return book;
	}

	private static void modifySubMenu() {
		
		Scanner sc = new Scanner(System.in);
		LibraryService libraryService = new LibraryService();
		
		do {
		System.out.println("===== 카테고리나 도서의 정보변경을 위한 메뉴입니다. =====");
		System.out.println("1. 카테고리 이름 변경하기");
		System.out.println("2. 도서의 정보를 변경하기");
		System.out.println("9. 이전 메뉴로 이동하기");
		System.out.print("원하시는 메뉴를 선택해주세요 : ");
		int no = sc.nextInt();
		switch(no) {
		case 1: libraryService.modifyCategory(inputChangeCategory()); break;
		case 2: libraryService.modifyBook(inputChangeBook()); break;
		}
		if(no == 9) {
			return;
		}
		}while(true);
		
	}

	private static Map<String, Object> inputChangeCategory() {
		Scanner sc = new Scanner(System.in);
		System.out.println("변경하려는 카테고리의 코드를 입력해주세요 : ");
		int categoryCode = sc.nextInt();
		sc.nextLine();
		System.out.println("변경할 카테고리 이름을 입력해주세요 : ");
		String categoryName = sc.nextLine();
		Map<String, Object> criteria = new HashMap<>();
		criteria.put("categoryCode", categoryCode);
		criteria.put("categoryName", categoryName);
		
		return criteria;
	}

	private static Map<String, Object> inputChangeBook() {
		
		Scanner sc = new Scanner(System.in);
		System.out.print("변경하고싶은 책의 코드를 입력하세요 : ");
		int code = sc.nextInt();
		sc.nextLine();
		System.out.print("책의 바꾸려는 이름을 입력하세요 : ");
		String name = sc.nextLine();
		System.out.print("책의 바꿀 저자이름을 입력하세요 : ");
		String author = sc.nextLine();
		System.out.print("책의 바꿀 가격을 입력하세요 : ");
		int price = sc.nextInt();
		System.out.print("변경할 카테고리 코드를 입력하세요 : ");
		int categoryCode = sc.nextInt();
		sc.nextLine();
		
		Map<String, Object> criteria = new HashMap<>();
		criteria.put("code", code);
		criteria.put("name", name);
		criteria.put("author", author);
		criteria.put("price", price);
		criteria.put("categoryCode", categoryCode);
		
		return criteria;
	}
	
	private static SearchCriteria inputSearchCriteria() {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("검색 기준을 입력해주세요(book or category) : ");
		String condition = sc.nextLine();
		System.out.println("검색어를 입력해주세요 : ");
		String value = sc.nextLine();
		
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setCondition(condition);
		searchCriteria.setValue(value);
	
		return searchCriteria;
	}
	
	
	private static Map<String, String> inputCategodyCode() {
		
		Scanner sc = new Scanner(System.in);
		System.out.print("수정하실 카테고리 코드를 입력하세요 : ");
		String categoryCode = sc.nextLine();
		System.out.print("수정하실 카테고리 이름을 입력하세요 : ");
		String categoryName = sc.nextLine();

		Map<String, String> parameter = new HashMap<>();
		parameter.put("categoryCode", categoryCode);
		parameter.put("categoryName", categoryName);
		
		return parameter;
	}
	
//	private static Map<String, String> inputBookCode() {
//
//		Scanner sc = new Scanner(System.in);
//		System.out.print("수정하실 책의 코드를 입력하세요 : ");
//		String bookCode = sc.nextLine();
//		System.out.print("수정하실 책의 이름을 입력하세요 : ");
//		String bookName = sc.nextLine();
//		System.out.print("수정하실 책의 저자를 입력하세요 : ");
//		String author = sc.nextLine();
//		System.out.print("수정하실 책의 가격을 입력하세요 : ");
//		String price = sc.nextLine();
//		
//		Map<String, String> parameter = new HashMap<>();
//		parameter.put("bookCode", bookCode);
//		parameter.put("bookName", bookName);
//		parameter.put("author", author);
//		parameter.put("price", price);
//		
//		return parameter;
//	}


	private static void deleteSubMenu() {
		
		Scanner sc = new Scanner(System.in);
		LibraryService libraryService = new LibraryService();
		
		do {
			System.out.println("1. 도서 삭제하기");
			System.out.println("9. 이전 단계로 이동하기");
			System.out.print("행동하실 번호를 선택해주세요. : ");
			int no = sc.nextInt();
			
		switch(no) {
			case 1: libraryService.deleteBook(inputDeleteBookCode()); break;
			}if(no == 9) {
				return;
			}
		} while(true);
	}

	private static Map<String, Object> inputDeleteBookCode() {
		
		Scanner sc = new Scanner(System.in);
		System.out.print("삭제할 도서 번호를 입력하세요 : ");
		String code = sc.nextLine();
	
		Map<String, Object> bookCode = new HashMap<>();
		bookCode.put("code", code);
		
		return bookCode;
	}

	/* 요까지 상목 코드 */
	
	private static void LanguageSection() {
		
		do {
			System.out.println("1.검색");
			System.out.println("2.추가");
			System.out.println("3.삭제");
			System.out.println("4.수정");
			System.out.println("9.돌아가기");
			int no = sc.nextInt();
	
			switch(no) {
				case 1 : Select(); break;
				case 2 : Insert(); break;
				case 3 :  break;
				case 4 :  break;
				case 9 :  return;
			}
		}while(true);
		
}
	private static void Select() {
		
		do {
			
			System.out.println("1.카테고리 보여주기");
			System.out.println("2.카테고리 별 도서보여주기(collection)");
			System.out.println("3.카테고리 랜덤박스(association)");
			System.out.println("9.돌아가기");

			int no = sc.nextInt();
			sc.nextLine();
			
			switch(no) {
			case 1 : libraryService.selectCategory(); break;
			case 2 : libraryService.selectCategoryBook((selectCategory())); 
			case 3 : libraryService.recommandFive((selectCategory())); 
			case 9 :  return;
			}
		}while(true);
	}

	private static int selectCategory() {
		
		libraryService.selectCategory();
		System.out.print("원하는 카테고리 번호 입력 : ");
		char selectCategory = sc.nextLine().charAt(0);
		Map<String, Object> getCategory = new HashMap<String, Object>();
		List<CategoryDTO> categoryList = libraryService.getSubCategory(selectCategory);
		if(categoryList.size() > 0 && categoryList != null) {
			System.out.print("세부 카테고리 번호 입력 : ");
			int categoryCode = sc.nextInt(); //세부 카테고리 선택
			sc.nextLine();
		return categoryCode;
		} else return 0;
		
	}

	private static void Insert() {

		
		do {
			
			System.out.println("1.원하는 카테고리 선택 + 메뉴 등록");
			System.out.println("2.카테고리추가");
			System.out.println("3.신규 카테고리 + 메뉴 등록");
			System.out.println("9.돌아가기");
			int no = sc.nextInt();
			sc.nextLine();
	
			switch(no) {
				case 1 : libraryService.AddBook((selectAdd())); break;
				case 2 : libraryService.AddCategory(inputCategory()); break;
				case 3 : libraryService.AddcategoryBook(inputBoth()); break;
				case 9 :  return;
			}
		}while(true);
	}

	


	private static BookCatergoryDTO inputBoth() {
		/* 새로운 카테고리를 추가하고 거기 책을 넣어줌 selectkey사용  */
		System.out.print("새로운 카테고리 이름 : ");
		String categoryName = sc.nextLine();
		System.out.print("책 이름 : ");
		String bookName = sc.nextLine();
		System.out.print("책 저자 : ");
		String author = sc.nextLine();
		System.out.print("책 가격 : ");
		int price = sc.nextInt();
//		int bookCode, String bookName, String author, int price, CategoryDTO category
		
		CategoryDTO category = new CategoryDTO();
		category.setCategoryName(categoryName);
		
		return new BookCatergoryDTO(price, bookName, author, price, category);
	}

	private static BookDTO selectAdd() {
		/* 책삽입(전체카테고리select->해당 세부카테고리select ->addBook) ->*/
		
		libraryService.selectCategory();
		System.out.print("원하는 카테고리 번호 입력 : ");
		char selectCategory = sc.nextLine().charAt(0);

		List<CategoryDTO> categoryList = libraryService.getSubCategory(selectCategory);
		if(categoryList.size() > 0 && categoryList != null) {
			System.out.print("세부 카테고리 번호 입력 : ");
			int categoryCode = sc.nextInt(); //세부 카테고리 선택
			sc.nextLine();
			BookDTO bookInfo = inputBookL();
			bookInfo.setCategoryCode(categoryCode);
			return bookInfo;
			
		} else return null;
	}
	
	private static BookDTO inputBookL() {
		
		System.out.println("도서를 추가합니다");
		System.out.print("책 이름 : ");
		String bookName = sc.nextLine();
		System.out.print("책 저자 : ");
		String author = sc.nextLine();
		System.out.print("책 가격 : ");
		int price = sc.nextInt();
		
		BookDTO bookInfo = new BookDTO();
		bookInfo.setBookName(bookName);
		bookInfo.setAuthor(author);
		bookInfo.setPrice(price);
		
		return bookInfo;
	}
	
	private static Map<String, String> inputCategory() {
		
		System.out.println("언어 카테고리를 추가합니다.");
		System.out.print("카테고리 이름 입력 : ");
		String categoryName = sc.nextLine();
		System.out.println("참조할 상위카테고리가 있습니까?(Y/N)");
		String checkRef = sc.nextLine();
		
		Map<String, String> categoryInfo = new HashMap<>();
		categoryInfo.put("categoryName", categoryName);
		categoryInfo.put("checkRef", checkRef);
//		 java.lang.NumberFormatException -> 가져올 값이 하나 이상이라 구분못함 or XML문에서 test='checkRef == "N"' 이렇게 해줘야 동작함..
		return categoryInfo;
}
	
	private static void literatureMenu() {
		
		Scanner sc = new Scanner(System.in);
		LibraryService libraryService = new LibraryService();
		do {
			System.out.println("============= 문학 서적 =============");
			System.out.println("1. 도서 조회");
			System.out.println("2. 도서 정보 추가");
			System.out.println("3. 도서 정보 수정");
			System.out.println("4. 도서 정보 삭제");
			System.out.println("9. 이전 메뉴로");
			System.out.print("원하는 메뉴의 번호를 입력해주세요 : ");
			int no = sc.nextInt();
			
			switch(no) {
				case 1 : libraryService.searchLiteratureBook(inputSearchLiterature()); break;
				case 2 : libraryService.registBook(inputBook()); break;
				case 3 : libraryService.modifyBook1(inputChangeInfo()); break;
				case 4 : libraryService.deleteBook1(inputBookCode()); break;
				case 9 : return;	
			}
			
		} while(true);
		
	}



	private static SearchCriteria inputSearchLiterature() {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("==================================");
		System.out.println("  종류             |  카테고리 코드");
		System.out.println("==================================");
		System.out.println("  한국문학          |  810  ");
		System.out.println("  중국문학          |  820  ");
		System.out.println("  일본문학          |  830  ");
		System.out.println("  영미문학          |  840  ");
		System.out.println("  독일문학          |  850  ");
		System.out.println("  프랑스문학         |  860  ");
		System.out.println("  스페인,포루투갈 문학 |  870  ");
		System.out.println("  이탈리아문학       |  880  ");
		System.out.println("  기타문학          |  890  ");
		System.out.println(" ");
		System.out.print("검색 조건을 입력하시겠습니까?(예 or 아니오) : ");
		
		boolean searchValue = "예".equals(sc.nextLine())? true: false;
		
		SearchCriteria searchCriteria = new SearchCriteria();
		if(searchValue) {
			
			System.out.print("검색할 카테고리 코드를 입력하세요 : ");
			String categoryCode = sc.nextLine();
			searchCriteria.setCondition("categoryCode");
			searchCriteria.setValue(categoryCode);
		}
		
		return searchCriteria;
	}

	private static Map<String, Object> inputBook() {
		
		Scanner sc = new Scanner(System.in);
		System.out.print("책 코드를 입력하세요 : ");
		int bookCode = sc.nextInt();
		System.out.print("책 이름을 입력하세요 : ");
		sc.nextLine();
		String bookName = sc.nextLine();		
		System.out.print("저자를 입력하세요 : ");
		String author = sc.nextLine();
		System.out.print("가격을 입력하세요 : ");
		int price = sc.nextInt();
		System.out.print("카테고리 코드를 입력하세요 : ");
		int categoryCode = sc.nextInt();
		
		Map<String, Object> criteria = new HashMap<>();
		criteria.put("bookCode", bookCode);
		criteria.put("bookName", bookName);
		criteria.put("author", author);
		criteria.put("price", price);
		criteria.put("categoryCode", categoryCode);
		return criteria;
	}

	private static Map<String, Object> inputChangeInfo() {
		
		Scanner sc = new Scanner(System.in);
		System.out.print("수정할 책 코드를 입력하세요 : ");
		int bookCode = sc.nextInt();
		System.out.print("수정할 책 이름을 입력하세요 : ");
		sc.nextLine();
		String bookName = sc.nextLine();
		System.out.print("수정할 저자를 입력하세요 : ");
		String author = sc.nextLine();
		System.out.print("수정할 가격을 입력하세요 : ");
		int price = sc.nextInt();
		System.out.print("수정할 카테고리 코드를 입력하세요 : ");
		int categoryCode = sc.nextInt();
		
		Map<String, Object> criteria = new HashMap<>();
		criteria.put("bookCode", bookCode);
		criteria.put("bookName", bookName);
		criteria.put("author", author);
		criteria.put("price", price);
		criteria.put("categoryCode", categoryCode);
		return criteria;		
	}	

	private static Map<String, Object> inputBookCode() {
		
		Scanner sc = new Scanner(System.in);
		System.out.print("삭제할 책 코드를 입력하세요 : ");
		int bookCode = sc.nextInt();
		Map<String, Object> criteria = new HashMap<>();
		criteria.put("bookCode", bookCode);
		
		return criteria;
	}
}

	

	
	
	
